package test.neosperience.com.thelword;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import test.neosperience.com.thelword.R;

import static android.support.v4.view.ViewCompat.animate;

public class PlayGameActivity extends Activity implements View.OnClickListener , View.OnLongClickListener{

    public static final float MAX_DIM = 1.5f;
    public static final int DEF_SIZE = 25;
    private ImageView icon1;
    private ImageView icon2;
    private ImageView icon3;
    private ImageView icon4;
    private TextView number1;
    private TextView number2;
    private TextView number3;
    private TextView number4;

    Handler myHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            return false;
        }
    });

    private float icon1Dim = 1;
    private float icon2Dim = 1;
    private float icon3Dim = 1;
    private float icon4Dim = 1;

    private static Activity activity;
    private Handler handler;

    public static void startPlayGameActivity (Activity activity){
        PlayGameActivity.activity = activity;
        ActivityOptions options = ActivityOptions
                .makeSceneTransitionAnimation(activity);
        Intent intent = new Intent(activity, PlayGameActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);

        handler = new Handler();

        icon1 = (ImageView) findViewById(R.id.icon1);
        icon2 = (ImageView) findViewById(R.id.icon2);
        icon3 = (ImageView) findViewById(R.id.icon3);
        icon4 = (ImageView) findViewById(R.id.icon4);
        number1 = (TextView) findViewById(R.id.number1);
        number2 = (TextView) findViewById(R.id.number2);
        number3 = (TextView) findViewById(R.id.number3);
        number4 = (TextView) findViewById(R.id.number4);

        icon1.setOnClickListener(this);
        icon2.setOnClickListener(this);
        icon3.setOnClickListener(this);
        icon4.setOnClickListener(this);
        icon1.setOnLongClickListener(this);
        icon2.setOnLongClickListener(this);
        icon3.setOnLongClickListener(this);
        icon4.setOnLongClickListener(this);

        number1.setTextSize(DEF_SIZE);
        number2.setTextSize(DEF_SIZE);
        number3.setTextSize(DEF_SIZE);
        number4.setTextSize(DEF_SIZE);

        final Runnable r = new Runnable()
        {
            public void run()
            {
                number1.setVisibility(View.VISIBLE);
                number2.setVisibility(View.VISIBLE);
                number3.setVisibility(View.VISIBLE);
                number4.setVisibility(View.VISIBLE);
            }
        };
        handler.postDelayed(r, 300);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.icon1:
                if(icon1Dim < MAX_DIM){
                    icon1Dim += 0.1f;
                    animate(icon1).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon1Dim).scaleY(icon1Dim).start();
                    setNumber(number1);
                }
                break;
            case R.id.icon2:
                if(icon2Dim < MAX_DIM) {
                    icon2Dim += 0.1f;
                    animate(icon2).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon2Dim).scaleY(icon2Dim).start();
                    setNumber(number2);
                }
                break;
            case R.id.icon3:
                if(icon3Dim < MAX_DIM) {
                    icon3Dim += 0.1f;
                    animate(icon3).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon3Dim).scaleY(icon3Dim).start();
                    setNumber(number3);
                }
                break;
            case R.id.icon4:
                if(icon4Dim < MAX_DIM) {
                    icon4Dim += 0.1f;
                    animate(icon4).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon4Dim).scaleY(icon4Dim).start();
                    setNumber(number4);
                }
                break;
        }
    }

    private void setNumber(TextView textView) {
        //check dpi size on getTextSize
        Float res =  textView.getTextSize() / 3 + 5f;
        int resText = Integer.parseInt((String) textView.getText()) + 1;
        textView.setTextSize(res);
        textView.setText(resText+"");
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()){
            case R.id.icon1:
                icon1Dim = 1f;
                number1.setText("1");
                animate(icon1).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon1Dim).scaleY(icon1Dim).start();
                number1.setTextSize(DEF_SIZE);
                break;
            case R.id.icon2:
                icon2Dim = 1f;
                number2.setText("1");
                animate(icon2).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon2Dim).scaleY(icon2Dim).start();
                number2.setTextSize(DEF_SIZE);
                break;
            case R.id.icon3:
                icon3Dim = 1f;
                number3.setText("1");
                animate(icon3).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon3Dim).scaleY(icon3Dim).start();
                number3.setTextSize(DEF_SIZE);
                break;
            case R.id.icon4:
                icon4Dim = 1f;
                number4.setText("1");
                animate(icon4).setDuration(1000).setInterpolator(new AnticipateOvershootInterpolator()).scaleX(icon4Dim).scaleY(icon4Dim).start();
                number4.setTextSize(DEF_SIZE);
                break;
        }

        return false;
    }

}
