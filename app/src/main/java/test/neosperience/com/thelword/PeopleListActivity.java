package test.neosperience.com.thelword;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.transition.Scene;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.Pair;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static test.neosperience.com.thelword.PlayGameActivity.startPlayGameActivity;


public class PeopleListActivity extends Activity {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerAdapter mAdapter;
    private CardView mCardEmpty;
    private Scene mScene1;
    private Scene mScene2;
    private TransitionManager mTransitionManager;
    private ImageView mBtnPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        // inside your activity
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        // set an exit transition
        getWindow().setExitTransition(new Explode());
        setContentView(R.layout.activity_people_list);

        ViewGroup container = (ViewGroup)findViewById(R.id.container);
        TransitionInflater transitionInflater = TransitionInflater.from(this);
        mTransitionManager = transitionInflater.inflateTransitionManager(R.transition.transition_manager, container);
        mScene1 = Scene.getSceneForLayout(container, R.layout.activity_scene_1, this);
        mScene2 = Scene.getSceneForLayout(container, R.layout.activity_scene_2, this);
        goToScene1();

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mCardEmpty = (CardView) findViewById(R.id.card_empty);
        mBtnPlus = (ImageView) findViewById(R.id.plus);

        // improve performance if you know that changes in content
        // do not change the size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new RecyclerAdapter(mRecyclerView);
        mAdapter.setOnItemClick(new RecyclerAdapter.OnItemClick() {
            @Override
            void onItemClick(View view, RecyclerAdapter.ViewHolder viewHolder, int position) {
                int childCount = mLayoutManager.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childView = mLayoutManager.getChildAt(i);
                    View button = childView.findViewById(android.R.id.button1);
                    EditText editText = (EditText) childView.findViewById(R.id.edit_text1);
                    TextView textView = (TextView) childView.findViewById(android.R.id.text1);
                    editText.setVisibility(View.GONE);
                    textView.setText(editText.getText());
                    textView.setVisibility(View.VISIBLE);

                    if (i == position) {
                        button.setVisibility(button.getVisibility() != View.VISIBLE ? View.VISIBLE : View.GONE);
                    } else {
                        button.setVisibility(View.GONE);
                    }
                }

//                CardView cardView = (CardView) view.findViewById(R.id.card_view);
//                ObjectAnimator anim = ObjectAnimator.ofFloat(cardView, "elevation", 0f, 30f);
//                anim.setDuration(2000);
//                anim.start();

            }
        });

        mAdapter.setOnItemEditClick(new RecyclerAdapter.OnItemEditClick() {
            @Override
            void onItemEditClick(final RecyclerAdapter.ViewHolder viewHolder, int position) {
                View childView = mLayoutManager.getChildAt(position);
                EditText editText = (EditText) childView.findViewById(R.id.edit_text1);
                TextView textView = (TextView) childView.findViewById(android.R.id.text1);
                Button ButtonView = (Button) childView.findViewById(android.R.id.button1);
                textView.setVisibility(View.GONE);
                ButtonView.setVisibility(View.GONE);
                editText.setText(textView.getText());
                editText.setVisibility(View.VISIBLE);

                viewHolder.mTextView.setVisibility(View.GONE);
                viewHolder.mEditTextView.setText(viewHolder.mTextView.getText());
                viewHolder.mEditTextView.setVisibility(View.VISIBLE);
                viewHolder.mButtonView.setVisibility(View.GONE);
//              Toast.makeText(PeopleListActivity.this, "Card " + viewHolder.mTextView.getText(), Toast.LENGTH_SHORT).show();
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        mCardEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPeople();
//                view_anim_invisible(mCardEmpty);
                goToScene2();
            }
        });

        if (mAdapter.getItemCount() == 0) {
            mRecyclerView.setVisibility(View.INVISIBLE);
            mCardEmpty.setVisibility(View.VISIBLE);
        }

    }

    public void onPlusClick(View view){
        addPeople();
    }

    public void goToScene1() {
        mTransitionManager.transitionTo(mScene1);
    }

    public void goToScene2() {
        mScene2.setEnterAction(new Runnable() {
            @Override
            public void run() {
                View plusButton = findViewById(R.id.plus);

                Outline outline = new Outline();
                outline.setOval(0, 0, plusButton.getWidth(), plusButton.getHeight());
                plusButton.setOutline(outline);

                ObjectAnimator anim = ObjectAnimator.ofFloat(plusButton, "elevation", 0f, 30f);
                anim.setDuration(2000);
                anim.start();
            }
        });
        mTransitionManager.transitionTo(mScene2);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    public void addPeople(){
        if(mAdapter.getItemCount() < 4){
            mAdapter.add(new PeopleDummy("Player " + mAdapter.getItemCount(), null));
        } else {
            //animation plus to "ok"
            int childCount = mLayoutManager.getChildCount();
            ArrayList<Pair> pairs = new ArrayList<Pair>();
            for (int i = 0; i < childCount; i++) {
                View childView = mLayoutManager.getChildAt(i);
                View icon = childView.findViewById(android.R.id.icon);
                pairs.add(Pair.create(icon, icon.getViewName()));
            }

            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(PeopleListActivity.this, pairs.get(0), pairs.get(1), pairs.get(2), pairs.get(3));
            Intent intent = new Intent(PeopleListActivity.this, PlayGameActivity.class);
            startActivity(intent, options.toBundle());
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.people_list, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            addPeople();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void view_anim_invisible(final View view) {

// get the center for the clipping circle
        int cx = (view.getLeft() + view.getRight()) / 2;
        int cy = (view.getTop() + view.getBottom()) / 2;

// get the initial radius for the clipping circle
        int initialRadius = view.getWidth();

// create the animation (the final radius is zero)
        ValueAnimator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);

// make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);
            }
        });

// start the animation
        anim.start();
    }

}
